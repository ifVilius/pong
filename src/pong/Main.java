package pong;

import java.io.IOException;
import java.util.Random;

public class Main {

    static boolean debug = false;

    /********************
    * 8 - ball spawn zone
    * 0 - empty space
    * 1 - contact/bounce zone
    * 2 - point zone
    * 3 - player1 pad, controls up - a,  down - d
    * 4 - player2 pad, controls up - j,  down - l
    * 9 - ball
    ********************/

    static int gameMatrix[][]={
            {2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,2},
            {2,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,2},
            {2,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
            {2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2},
    };

    static Random rand = new Random();
    static int gameHeight = gameMatrix.length;
    static int gameWidth = gameMatrix[0].length;
    static int player1Y, player1X, player2Y, player2X;
    static int player1Score = 0, player2Score = 0, scoreLimit = 2;
    static boolean ballSpawned = false, touchedBottomSide = false,
            touchedTopSide = false, touchedP1Paddle = false, touchedP2Paddle = false,
    touchedTopPaddle = false, touchedMiddlePaddle = false, touchedBottomPaddle = false;
    static int ballPosY, ballPosX, ballStartX, direction, lastDirection, angle;

    public static void main(String[] args) throws IOException {

        while(true){

            //show score
            System.out.print("\t\tP1: " + player1Score + "\t\tP2: " + player2Score);
            System.out.println();

            //game over
            if (player1Score == scoreLimit) {
                System.out.println("\t\tPlayer 1 WINS!");
                System.exit(0);
            } else if (player2Score == scoreLimit) {
                System.out.println("\t\tPlayer 2 WINS!");
                System.exit(0);
            }

            //draw game objects
            for (int y = 0; y<gameHeight; y++){
                for (int x = 0; x<gameWidth; x++){
                    if (gameMatrix[y][x] == 1){ //bounce zone
                        System.out.print("-");
                    } else if (gameMatrix[y][x] == 9){ //ball
                        System.out.print("*");
                    } else if (gameMatrix[y][x] == 8){ //center line
                        ballStartX = x;
                        System.out.print("|");
                    } else if (gameMatrix[y][x] == 3){ //p1 paddle
                        System.out.print("|");
                        player1Y = y;
                        player1X = x;
                    } else if (gameMatrix[y][x] == 4){ //p2 paddle
                        System.out.print("|");
                        player2Y = y;
                        player2X = x;
                    } else { //empty space and score zone
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }

            //spawn the ball
            if (!ballSpawned) {
                System.out.println("\tPress any key to start..");
                ballPosX = ballStartX;
                ballPosY = rand.nextInt(gameHeight - 2) + 1;
                gameMatrix[ballPosY][ballPosX] = 9;
                angle = rand.nextInt(3) + 1; //random angle: 1 - straight, 2 - diagonally up, 3 - diagonally down
                 //direction 1 - left, 2 - right
                if ((player1Score + player2Score) == 0) { //p1 starts the game
                    direction = 2;
                } else if (player1Score > player2Score) { //p1 wins, his ball
                    direction = 2;
                } else if (player2Score > player1Score) { //p2 wins, his ball
                    direction = 1;
                } else { //score is equal, ball is last scored player
                    direction = lastDirection;
                }
                ballSpawned = true;
            } else { //move the ball

                //leave touched objects on map
                if (ballPosX == ballStartX) {
                    gameMatrix[ballPosY][ballPosX] = 8;
                }  else if (gameMatrix[ballPosY][ballPosX] == 1) {
                    gameMatrix[ballPosY][ballPosX] = 1;
                } else if (gameMatrix[ballPosY][ballPosX] == 3) {
                    gameMatrix[ballPosY][ballPosX] = 3;
                } else if (gameMatrix[ballPosY][ballPosX] == 4) {
                    gameMatrix[ballPosY][ballPosX] = 4;
                } else {
                    gameMatrix[ballPosY][ballPosX] = 0; //remove old ball position
                }

                //events, ball moving trajectories
                if (touchedTopSide) {
                    if (direction == 1) { //from right
                        ballPosY++;
                        ballPosX--;
                    } else { //from left
                        ballPosY++;
                        ballPosX++;
                    }
                } else if (touchedBottomSide) {
                    if (direction == 1) { //from right
                        ballPosY--;
                        ballPosX--;
                    } else { //from left
                        ballPosY--;
                        ballPosX++;
                    }
                } else if (touchedP1Paddle) {
                    if (touchedTopPaddle) {
                        ballPosY--;
                        ballPosX++;
                    } else if (touchedMiddlePaddle) {
                        ballPosX++;
                    } else {
                        ballPosY++;
                        ballPosX++;
                    }
                } else if (touchedP2Paddle) {
                    if (touchedTopPaddle) {
                        ballPosY--;
                        ballPosX--;
                    } else if (touchedMiddlePaddle) {
                        ballPosX--;
                    } else {
                        ballPosY++;
                        ballPosX--;
                    }
                } else {
                    //get new ball position
                    if (direction == 1) { //p2 started to left
                        if (angle == 1) { //straight
                            ballPosX--;
                        } else if (angle == 2) { //diagonally up
                            ballPosY--;
                            ballPosX--;
                        } else { //diagonally down
                            ballPosY++;
                            ballPosX--;
                        }
                    } else { //p1 started to right
                        if (angle == 1) { //straight
                            ballPosX++;
                        } else if (angle == 2) { //diagonally up
                            ballPosY--;
                            ballPosX++;
                        } else { //diagonally down
                            ballPosY++;
                            ballPosX++;
                        }
                    }
                }

                //collision detection
                if (gameMatrix[ballPosY][ballPosX] == 0 || gameMatrix[ballPosY][ballPosX] == 8) { //move ball further
                    gameMatrix[ballPosY][ballPosX] = 9;
                } else if (gameMatrix[ballPosY][ballPosX] == 1) { //touched top or bottom
                    if (ballPosY > gameHeight/2) {
                        touchedBottomSide = true;
                        touchedTopSide = false;
                        touchedP1Paddle = false;
                        touchedP2Paddle = false;
                    }  else {
                        touchedTopSide = true;
                        touchedBottomSide = false;
                        touchedP1Paddle = false;
                        touchedP2Paddle = false;
                    }
                } else if (gameMatrix[ballPosY][ballPosX] == 2) { //p1 or p2 scored
                    ballSpawned = false;
                    touchedTopSide = false;
                    touchedBottomSide = false;
                    touchedP1Paddle = false;
                    touchedP2Paddle = false;
                    if (ballPosX > gameWidth/2) {
                        player1Score++;
                        lastDirection = 2;
                    } else {
                        player2Score++;
                        lastDirection = 1;
                    }
                } else if (gameMatrix[ballPosY][ballPosX] == 3) { //touched p1 paddle
                    direction = 2;
                    touchedP1Paddle = true;
                    touchedTopSide = false;
                    touchedBottomSide = false;
                    touchedP2Paddle = false;
                    touchedTopPaddle = false;
                    touchedMiddlePaddle = false;
                    touchedBottomPaddle = false;
                    if (ballPosY == player1Y-2) {
                        touchedTopPaddle = true;
                    } else if (ballPosY == player1Y-1) {
                        touchedMiddlePaddle = true;
                    } else {
                        touchedBottomPaddle = true;
                    }
                } else if  (gameMatrix[ballPosY][ballPosX] == 4) { //touched p2 paddle
                    direction = 1;
                    touchedP2Paddle = true;
                    touchedTopSide = false;
                    touchedBottomSide = false;
                    touchedP1Paddle = false;
                    touchedTopPaddle = false;
                    touchedMiddlePaddle = false;
                    touchedBottomPaddle = false;
                    if (ballPosY == player2Y-2) {
                        touchedTopPaddle = true;
                    } else if (ballPosY == player2Y-1) {
                        touchedMiddlePaddle = true;
                    } else {
                        touchedBottomPaddle = true;
                    }
                }

                if (debug) {
                    System.out.println("\n| direction: " + direction + ", angle:" + angle +
                            "\n| top: " + touchedTopSide + ", bottom: " + touchedBottomSide +
                            "\n| p1: " + touchedP1Paddle + ", p2: " + touchedP2Paddle +
                            "\n| pTop: " + touchedTopPaddle + ", pMiddle: " + touchedMiddlePaddle + ", pBottom: " + touchedBottomSide +
                            "\n| p1x: " + player1X + " p1y: " + player1Y + ", p2x: " + player2X + " p2y: " + player2Y + "\n");
                }

            } //end move the ball

            //move p1 & p2 paddles
            switch(System.in.read()){
                case 'a':
                    if (player1Y -3 >= 1) {
                        gameMatrix[player1Y][player1X] = 0;
                        gameMatrix[player1Y-3][player1X] = 3;
                    } break;
                case 'd':
                    if (player1Y +2 < gameHeight) {
                        gameMatrix[player1Y-2][player1X] = 0;
                        gameMatrix[player1Y+1][player1X] = 3;
                    } break;
                case 'j':
                    if (player2Y -3 >= 1) {
                        gameMatrix[player2Y][player2X] = 0;
                        gameMatrix[player2Y-3][player2X] = 4;
                    } break;
                case 'l':
                    if (player2Y +2 < gameHeight) {
                        gameMatrix[player2Y-2][player2X] = 0;
                        gameMatrix[player2Y+1][player2X] = 4;
                    } break;
                case 'q': System.exit(0);
            }
        }

    }
}
